using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using System.Collections.Generic;
using Unity.Entities;

public class NetworkManager : MonoBehaviourPunCallbacks
{
    [SerializeField] private List<GameObject> _players;

    private void Start()
    {
        PhotonNetwork.ConnectUsingSettings();
    }

    public override void OnConnectedToMaster()
    {
        RoomOptions options = new RoomOptions()
        {
            MaxPlayers = 2,
            IsVisible = false
        };
        PhotonNetwork.JoinOrCreateRoom("test", options, TypedLobby.Default);
    }

    public override void OnJoinedRoom()
    {
        var id = PhotonNetwork.LocalPlayer.ActorNumber;
        Debug.Log(id);
        if (id > _players.Count)
            Debug.Log("You are spectator!");
        else
        {
            GameObject player = _players[id - 1];
            EntityManager dstManager = World.DefaultGameObjectInjectionWorld.EntityManager;
            if (player.TryGetComponent(out EntityStorage entityStorage))
            {
                dstManager.SetComponentData(entityStorage.EntityByGameObject, new PhotonIdData()
                {
                    photonId = id
                });
            }
            if(player.TryGetComponent(out PhotonView pv))
            {
                pv.TransferOwnership(PhotonNetwork.LocalPlayer);
            }
        }        
    }
}
