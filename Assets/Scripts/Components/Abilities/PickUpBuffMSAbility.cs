using Photon.Pun;
using Photon.Pun.Demo.Cockpit;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

public class PickUpBuffMSAbility : MonoBehaviour, IAbilityTarget, IConvertGameObjectToEntity
{
    [SerializeField] private float _buffMoveSpeedMultiplier = 2.0f;
    private bool _isExecuted;
    private EntityManager _dstManager;
    private Entity _entity;

    public List<GameObject> Targets { get; set; }

    public void Execute(Entity entity)
    {
        foreach (var target in Targets)
        {
            if (!_isExecuted)
            {
                _isExecuted = true;
                var character = target.GetComponent<CharacterConvertToEntity>();
                if (character == null) return;
                character.SpeedMultiplier = _buffMoveSpeedMultiplier;
                if (TryGetComponent(out PhotonView pv))
                {
                    pv.RPC("RPCPickUp", RpcTarget.All);
                }
            }
        }
    }

    [PunRPC]
    public void RPCPickUpBuff()
    {
        Destroy(gameObject);
        _dstManager.DestroyEntity(_entity);
    }


    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {
        _dstManager = dstManager;
        _entity = entity;
    }
}
