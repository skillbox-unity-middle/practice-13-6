﻿using Photon.Pun;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

public class ShootAbility : MonoBehaviour, IAbility, IConvertGameObjectToEntity, IDeclareReferencedPrefabs, IPunObservable
{
    [SerializeField] private GameObject _bullet;
    [SerializeField] private float _shootDelay;

    private static Entity _prefabEntity;
    private EntityManager _dstManager;

    private float _shootTime = float.MinValue;
    private float _offsetY = 1.2f;

    public float ShootDelayMultiplier { set => _shootDelay += value; }

    public void Execute(Entity entity)
    {
        if (Time.time < _shootTime + _shootDelay) return;
        _shootTime = Time.time;

        if (_bullet != null)
        {
            var t = transform;
            var pos = new Vector3(t.position.x, t.position.y + _offsetY, t.position.z);
            var rotation = t.rotation;

            var newBullet = _dstManager.Instantiate(_prefabEntity);

            _dstManager.SetComponentData(newBullet, new Translation { Value = pos });
            _dstManager.SetComponentData(newBullet, new Rotation { Value = rotation });
            _dstManager.SetComponentData(newBullet, new RotatedData()
            {
                isRotated = false
            });

            if (TryGetComponent(out PhotonView pv))
            {
                pv.RPC("RPCInstantiateBullet", RpcTarget.Others);
            }
        }
        else
        {
            Debug.Log("Need to add link to Bullet prefab!");
        }
    }

    [PunRPC]
    public void RPCInstantiateBullet()
    {
            var t = transform;
            var pos = new Vector3(t.position.x, t.position.y + _offsetY, t.position.z);
            var rotation = t.rotation;

            var newBullet = _dstManager.Instantiate(_prefabEntity);

            _dstManager.SetComponentData(newBullet, new Translation { Value = pos });
            _dstManager.SetComponentData(newBullet, new Rotation { Value = rotation });
            _dstManager.SetComponentData(newBullet, new RotatedData()
            {
                isRotated = false
            });
    }

    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {
        _dstManager = dstManager;
        Entity entityPrefab = conversionSystem.GetPrimaryEntity(_bullet);
        _prefabEntity = entityPrefab;
    }

    public void DeclareReferencedPrefabs(List<GameObject> referencedPrefabs)
    {
        referencedPrefabs.Add(_bullet);
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(_shootDelay);
        }
        else
        {
            _shootDelay = (float)stream.ReceiveNext();
        }
    }
}

public class EntityPrefabConversionSystem : GameObjectConversionSystem
{
    protected override void OnUpdate()
    {

    }
}

public struct RotatedData : IComponentData
{
    public bool isRotated;
}
