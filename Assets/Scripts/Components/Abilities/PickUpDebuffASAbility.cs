using Photon.Pun;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

public class PickUpDebuffASAbility : MonoBehaviour, IAbilityTarget, IConvertGameObjectToEntity
{
    [SerializeField] private float _debuffShootDelay = 0.5f;
    private bool _isExecuted;
    private EntityManager _dstManager;
    private Entity _entity;

    public List<GameObject> Targets { get; set; }

    public void Execute(Entity entity)
    {
        foreach (var target in Targets)
        {
            if (!_isExecuted)
            {
                _isExecuted = true;
                var shootAbility = target.GetComponent<ShootAbility>();
                if (shootAbility == null) return;
                shootAbility.ShootDelayMultiplier = _debuffShootDelay;
                if (TryGetComponent(out PhotonView pv))
                {
                    pv.RPC("RPCPickUp", RpcTarget.All);
                }
            }
        }
    }

    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {
        _dstManager = dstManager;
        _entity = entity;
    }
}
