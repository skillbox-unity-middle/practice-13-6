﻿using Photon.Pun;
using Unity.Entities;
using UnityEngine;

public class RPCPickUpper : MonoBehaviour, IConvertGameObjectToEntity
{
    private Entity _entity;
    private EntityManager _dstManager;

    [PunRPC]
    public void RPCPickUp()
    {
        Destroy(gameObject);
        _dstManager.DestroyEntity(_entity);
    }

    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {
        _entity = entity;
        _dstManager = dstManager;
    }
}