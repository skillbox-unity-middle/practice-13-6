using Photon.Pun;
using System.Collections.Generic;
using UnityEngine;

public class CharacterData : MonoBehaviour, IPunObservable
{
    public List<MonoBehaviour> levelUpActions;
    public GameObject inventoryUIRoot;

    [SerializeField] private int _currentLevel = 1;
    [SerializeField] private int _score = 0;
    [SerializeField] private int _scoreToNextLevel = 20;
    
    public int CurrentLevel => _currentLevel;
    public int Score => _score;
    public int ScoreToNextLevel => _scoreToNextLevel;

    public void AddScore(int score)
    {
        _score += score;
        if (_score >= _scoreToNextLevel) LevelUp();
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(_currentLevel);
            stream.SendNext(_score);
            stream.SendNext(_scoreToNextLevel);
        }
        else
        {
            _currentLevel = (int)stream.ReceiveNext();
            _score = (int)stream.ReceiveNext();
            _scoreToNextLevel = (int)stream.ReceiveNext();
        }
    }

    private void LevelUp()
    {
        _currentLevel++;
        _score = 0;
        _scoreToNextLevel *= 2;
        foreach (var action in levelUpActions)
        {
            if (!(action is ILevelUp levelUp)) return;
            levelUp.LevelUp(this, _currentLevel);
        }
    }
}
