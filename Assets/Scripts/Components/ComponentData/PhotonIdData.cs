﻿using Unity.Entities;

public struct PhotonIdData : IComponentData
{
    public int photonId;
}