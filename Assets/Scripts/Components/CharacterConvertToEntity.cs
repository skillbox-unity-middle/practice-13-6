using Photon.Pun;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

public class CharacterConvertToEntity : MonoBehaviour, IConvertGameObjectToEntity, IPunObservable
{
    [SerializeField] private float2 _speed = new float2(0.1f, 0.1f);

    private Entity _entity;
    private EntityManager _dstManager;

    private float _speedX
    {
        get => _speed.x;
        set => _speed.x = value;
    }
    private float _speedY
    {
        get => _speed.y;
        set => _speed.y = value;
    }

    public float2 SpeedMultiplier { 
        set
        {
            _speed *= value;
            _dstManager.AddComponentData(_entity, new MoveData
            {
                speed = _speed
            });
        }
    }

    public MonoBehaviour dashAction;
    public MonoBehaviour shootAction;

    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {
        if (gameObject.TryGetComponent(out EntityStorage entityStorage))
        {
            entityStorage.EntityByGameObject = entity;
            dstManager.AddComponentData(entity, new PhotonIdData());
        }

        dstManager.AddComponentData(entity, new InputData());

        dstManager.AddComponentData(entity, new MoveData { 
            speed = _speed
        });
        if(dashAction != null && dashAction is IAbility)
        {
            dstManager.AddComponentData(entity, new DashData() { 
                isDashAvalaible = true
            });
        }
        if (shootAction != null && shootAction is IAbility)
        {
            dstManager.AddComponentData(entity, new ShootData());
        }
        _entity = entity;
        _dstManager = dstManager;
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(_speedX);
            stream.SendNext(_speedY);
        }
        else
        {
            _speedX = (float)stream.ReceiveNext();
            _speedY = (float)stream.ReceiveNext();
        }
    }
}
